 #include "PrefixTree.hpp"

int main()
{
    PrefixTree<std::string> tree;
    const std::vector<std::vector<std::string>> vec {
        {"very", "nice", "tree"},
        {"very", "good", "algorithm"},
        {"can", "someone", "explain", "how", "does", "it", "work"},
        {"can", "of", "coke"}
    };
    for (auto const & r : vec) tree.insert(r);
    if (auto res { tree.find({"can"}) }; res)
    {
        res->get().print();
    }
    return 0;
}
