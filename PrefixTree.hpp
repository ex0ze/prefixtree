#ifndef PREFIXTREE_H
#define PREFIXTREE_H

#include <map>
#include <iterator>
#include <iostream>     //! ostream_iterator <> {std::cout}
#include <list>
#include <optional>     //! find ret
#include <functional>   //! std::cref

template <typename T>
class PrefixTree
{
public:
    PrefixTree() {}
    template<class InputIterator>
    PrefixTree(InputIterator beg, InputIterator end)
    {
        insert(beg, end);
    }
    PrefixTree(const std::initializer_list<T>& list)
    {
        insert(std::cbegin(list), std::cend(list));
    }
    PrefixTree(const PrefixTree&) = default;
    PrefixTree(PrefixTree&&) noexcept = default;
    PrefixTree& operator=(const PrefixTree& other) = default;
    PrefixTree& operator=(PrefixTree&& other) noexcept = default;
    template<class InputIterator>
    void insert(InputIterator beg, InputIterator end)
    {
        if (beg == end)
            return;

        m_trees[*beg].insert(std::next(beg), end);
    }
    template<class Container>
    void insert(const Container& c)
    {
        insert(std::begin(c), std::end(c));
    }
    void insert(const std::initializer_list<T>& list)
    {
        insert(std::begin(list), std::end(list));
    }
    template<class InputIterator>
    std::optional<std::reference_wrapper<const PrefixTree>>
    find(InputIterator beg, InputIterator end) const
    {
        if (beg == end)
            return std::cref(*this);
        auto findIter = m_trees.find(*beg);
        if (findIter == std::cend(m_trees)) return {};
        return (*findIter).second.find(std::next(beg), end);
    }
    template<class Container>
    std::optional<std::reference_wrapper<const PrefixTree>>
    find(const Container& c) const
    {
        return find(std::begin(c), std::end(c));
    }
    std::optional<std::reference_wrapper<const PrefixTree>>
    find(const std::initializer_list<T>& list) const
    {
        return find(std::begin(list), std::end(list));
    }
    void clear()
    {
        m_trees.clear();
    }
    void print(std::list<T>& l) const
    {
        if (m_trees.empty())
        {
            std::copy(std::cbegin(l), std::cend(l), std::ostream_iterator<T>{std::cout, " "});
            std::cout << std::endl;
        }
        for (const auto& t : m_trees)
        {
            l.push_back(t.first);
            t.second.print(l);
            l.pop_back();
        }
    }
    void print() const
    {
        std::list<T> l;
        print(l);
    }
private:
    std::map<T, PrefixTree> m_trees;
};

#endif // PREFIXTREE_H
